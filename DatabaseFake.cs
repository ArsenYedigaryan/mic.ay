﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Group
{
    static class DatabaseFake
    {
      
       static public List<Students> CreateStudent(int count)
        {
            List<Students> list = new List<Students>(count);
            Random rand = new Random();
            for (int i = 0; i < count; i++)
            {
                Students st = new Students();
                st.name = $"A{i}";
                st.surname = $"B{i}yan";

                list.Add(st);
            }
            return list;
        }
       static public List<Teachers> CreateTeacher(int count)
        {
            List<Teachers> list = new List<Teachers>(count);
            Random rand = new Random();
            for (int i = 0; i < count; i++)
            {
                Teachers tc = new Teachers();
                tc.name = $"A{i}";
                tc.surname = $"B{i}yan";

                list.Add(tc);
            }
            return list;
        }

    }
}
