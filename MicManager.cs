﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Group
{
    class MicManager
    {
        static void Shuffle(List<Students> list)
        {
            Random rand = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rand.Next(n + 1);
                Students str = list[k];
                list[k] = list[n];
                list[n] = str;
            }
        }

        static void Shuffle(List<Teachers> list)
        {
            Random rand = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rand.Next(n + 1);
                Teachers str = list[k];
                list[k] = list[n];
                list[n] = str;
            }
        }

    }
}
